#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char tagtofind[]={0x9,0x62,0x9};
char matchbuf[3];
char outputbuf[33];

int matchword()
{
	char i;
	for(i=0;i<3;i++){
		if(matchbuf[i]!=tagtofind[i])
			return 0;
	}
	return 1;
}

void routput()
{
	char caddr[7];
	char jaddr[7];
	char i;
	memcpy(caddr,&outputbuf[1],7);
	memcpy(jaddr,&outputbuf[25],7);
	for(i=0;i<7;i++){
		if(caddr[i]!=jaddr[i])
			return;
	}
	printf("%s",outputbuf);
}

int main()
{
	FILE *fp_match;
	FILE *fp_pick;
	char rtv_matchword;
	long position_fp_match;
	if(!(fp_match=fopen("E:\\attasm.txt","rt"))){
		printf("file open error");
		getchar();
		exit(0);
	}
	if(!(fp_pick=fopen("E:\\attasm.txt","rt"))){
		printf("file open error");
		getchar();
		exit(0);
	}
	for(;;){
		fread(matchbuf,3,1,fp_match);
		if(feof(fp_match))
			break;
		fseek(fp_match,-2,SEEK_CUR);
		rtv_matchword=matchword();
		if(rtv_matchword==1){
			fseek(fp_pick,ftell(fp_match)-20,SEEK_SET);
			fread(outputbuf,33,1,fp_pick);
			routput();
		}
	}
	fclose(fp_match);
	fclose(fp_pick);
	printf("complete");
	getchar();
}
