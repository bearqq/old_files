#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char portnum[5];

char txtbuf[25];

char newline[]={10};

void readconf()
{
	FILE *fp_conf;
	if(!(fp_conf=fopen(".\\portconf.inf","rt"))){
		printf("port config file do not exist");
		getchar();
		exit(0);
	}
	fread(portnum,5,1,fp_conf);
	fclose(fp_conf);
}

void addstr()
{
	txtbuf[strlen(txtbuf)]=':';
	memcpy(&txtbuf[strlen(txtbuf)],portnum,strlen(portnum));
	strcpy(&txtbuf[strlen(txtbuf)],newline);
}

int main()
{
	char i;
	char tmp[1];
	FILE *fp_src;
	FILE *fp_dst;
	if(!(fp_src=fopen(".\\ip.txt","rt"))){
		printf("file open error");
		getchar();
		exit(0);
	}
	if(!(fp_dst=fopen(".\\ipmod.txt","wt"))){
		printf("file create error");
		getchar();
		exit(0);
	}
	readconf();
	for(;;){
		for(i=0;;i++){
			fread(tmp,1,1,fp_src);
			if(tmp[0]==10)
				break;
			if(feof(fp_src)){
				i=30;
				break;
			}
			txtbuf[i]=tmp[0];
		}
		addstr();
		fwrite(txtbuf,strlen(txtbuf),1,fp_dst);
		if(i==30)
			break;
		memset(txtbuf,0,strlen(txtbuf));
	}
	fclose(fp_src);
	fclose(fp_dst);
	printf("complete");
	getchar();
}
