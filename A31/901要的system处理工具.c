#include <stdio.h>
#include <stdlib.h>

typedef unsigned int uint;

typedef struct {
    uint magic;
    uint count;
    uint size;
} SectionInfo;

int main(int argc, char *argv[])
{
    SectionInfo info;
    uint count, filesize;
    void *pmem;
    FILE *inFile, *outFile;

    if (argc != 2)
    {
        printf("usage: %s filename\n", argv[0]);
        system("pause\n");
        return -1;
    }

    inFile = fopen(argv[1], "rb");
    if (inFile == NULL)
    {
        printf("cannot open file %s\n", argv[1]);
        system("pause\n");
        return -2;
    }

    fseek(inFile, 0, SEEK_END);
    filesize = ftell(inFile);

    outFile = fopen("system2.img", "wb");

    fseek(inFile, 0x1C, SEEK_SET);
    do {
        fread(&info, 1, sizeof(info), inFile);
        if (info.magic == 0x0000CAC1)
        {
            count = info.size - sizeof(info);
            pmem = malloc(count);
            fread(pmem, 1, count, inFile);
            fwrite(pmem, 1, count, outFile);
            free(pmem);
        }
        else if (info.magic == 0x0000CAC3)
        {
            fseek(outFile, info.count * 0x1000-1, SEEK_CUR);
            count = 0;
            fwrite(&count, 1, 1, outFile);
        }
        else
            break;


    }
    while (ftell(inFile)<filesize);

    fclose(inFile);
    fclose(outFile);

    system("pause\n");
    return 0;
}
