#include <stdio.h>
#include <stdlib.h>
typedef unsigned int uint;

typedef struct {
    uint magic;
    uint count;
    uint size;
} SectionInfo;

int main(int argc, char *argv[])
{
    FILE *fp1,*fp2;
    char fram[1000001];
    unsigned long pt,lth,strt,sk;
    short search;

    //system
    SectionInfo info;
    uint count, filesize;
    void *pmem;
    FILE *inFile, *outFile;
    //system

    printf("A31/A10 diy tool by 昂达玩家固件小组：");
    printf("%c%c%c%c%c%c%c%c%c%c%c",105+10,100+10,47+50,67+40,81+20,100+3,113+3,119+1,50+7,40+8,30+19);
    printf(" ");
    printf("%c%c%c%c%c%c",94+4,31+70,27+70,134-20,103+10,119-6);
    printf(" and Gary\n特别向 全志Allwinner 致敬\n整个过程耗时较长，请保持耐心。\n\n");

    if ((argc==1) || (argv[1]=="A31"))
        {


        if((fp1=fopen("update.img","rb+"))==NULL)
        {
            printf("File update.img cannot be opened\n");
            exit(0);
        }
        else
            printf("File opened for reading\n");

        if((fp2=fopen("boot.img","wb"))==NULL)
        {
            printf("File cannot be opened\n");
            exit(0);
        }
        else
            printf("File opened for writing\n");

        //pt=0x10000;
        pt=0;
        search=0;

        fseek(fp1,pt,0);
        fread(&sk,1,1,fp1);
        while (search!=1)
        {
            while (sk!='b')
            {
                pt++;
                fseek(fp1,pt,0);
                fread(&sk,1,1,fp1);
            }
            //printf("r found %X\n",pt);

            fseek(fp1,(pt+1),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='o');
            fseek(fp1,(pt+2),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='o');
            fseek(fp1,(pt+3),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='t');
            fseek(fp1,(pt+4),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='.');
            fseek(fp1,(pt+5),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='f');
            fseek(fp1,(pt+6),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='e');
            fseek(fp1,(pt+7),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='x')-6;
            if (search!=1)
            {
                search=0;
                pt=pt+1;
                fseek(fp1,pt,0);
                fread(&sk,1,1,fp1);
            }

        }
        //printf("Boot.img found %X\n",pt); //printf("STR found %X\n",pt);
        //getch();

        lth=0;
        fseek(fp1,(pt+0x100),0);
        fread(&sk,1,1,fp1);
        lth=lth+sk;
        fread(&sk,1,1,fp1);
        lth=lth+sk*0x100;
        fread(&sk,1,1,fp1);
        lth=lth+sk*0x10000;
        fread(&sk,1,1,fp1);
        lth=lth+sk*0x1000000;
        if (sk==0) lth=lth%0x1000000;
        //printf("lth found %X\n",lth);
        //getch();

        strt=0;
        fseek(fp1,(pt+0x100+0x10),0);
        fread(&sk,1,1,fp1);
        strt=strt+sk;
        fread(&sk,1,1,fp1);
        strt=strt+sk*0x100;
        fread(&sk,1,1,fp1);
        strt=strt+sk*0x10000;
        fread(&sk,1,1,fp1);
        strt=strt+sk*0x1000000;
        if (sk==0) strt=strt%0x1000000;
        //printf("Strt found %X\n",strt);
        //getch();


        fseek(fp1,strt,0);
        while (lth>1000000)
        {
            fread(&fram,1000000,1,fp1);
            fwrite(&fram,1000000,1,fp2);
            lth=lth-1000000;
        }
        fread(&fram,lth,1,fp1);
        fwrite(&fram,lth,1,fp2);
        fclose(fp2);

        printf("Boot.img is ready!\n");




        if((fp2=fopen("bootloader.img","wb"))==NULL)
        {
            printf("File cannot be opened\n");
            exit(0);
        }
        else
            printf("File opened for writing\n");

        //pt=0x10000;
        pt=0;
        search=0;

        fseek(fp1,pt,0);
        fread(&sk,1,1,fp1);
        while (search!=1)
        {
            while (sk!='b')
            {
                pt++;
                fseek(fp1,pt,0);
                fread(&sk,1,1,fp1);
            }
            //printf("r found %X\n",pt);

            fseek(fp1,(pt+1),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='o');
            fseek(fp1,(pt+2),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='o');
            fseek(fp1,(pt+3),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='t');
            fseek(fp1,(pt+4),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='l');
            fseek(fp1,(pt+5),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='o');
            fseek(fp1,(pt+6),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='a');
            fseek(fp1,(pt+7),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='d');
            fseek(fp1,(pt+8),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='e');
            fseek(fp1,(pt+9),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='r');
            fseek(fp1,(pt+10),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='.');
            fseek(fp1,(pt+11),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='f');
            fseek(fp1,(pt+12),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='e');
            fseek(fp1,(pt+13),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='x')-12;
            if (search!=1)
            {
                search=0;
                pt=pt+1;
                fseek(fp1,pt,0);
                fread(&sk,1,1,fp1);
            }

        }
        //printf("Bootloader.img found %X\n",pt); //printf("STR found %X\n",pt);
        //getch();

        lth=0;
        fseek(fp1,(pt+0x100),0);
        fread(&sk,1,1,fp1);
        lth=lth+sk;
        fread(&sk,1,1,fp1);
        lth=lth+sk*0x100;
        fread(&sk,1,1,fp1);
        lth=lth+sk*0x10000;
        fread(&sk,1,1,fp1);
        lth=lth+sk*0x1000000;
        if (sk==0) lth=lth%0x1000000;
        //printf("lth found %X\n",lth);
        //getch();

        strt=0;
        fseek(fp1,(pt+0x100+0x10),0);
        fread(&sk,1,1,fp1);
        strt=strt+sk;
        fread(&sk,1,1,fp1);
        strt=strt+sk*0x100;
        fread(&sk,1,1,fp1);
        strt=strt+sk*0x10000;
        fread(&sk,1,1,fp1);
        strt=strt+sk*0x1000000;
        if (sk==0) strt=strt%0x1000000;
        //printf("Strt found %X\n",strt);
        //getch();


        fseek(fp1,strt,0);
        while (lth>1000000)
        {
            fread(&fram,1000000,1,fp1);
            fwrite(&fram,1000000,1,fp2);
            lth=lth-1000000;
        }
        fread(&fram,lth,1,fp1);
        fwrite(&fram,lth,1,fp2);
        fclose(fp2);

        printf("Bootloader.img is ready!\n");



        if((fp2=fopen("recovery.img","wb"))==NULL)
        {
            printf("File cannot be opened\n");
            exit(0);
        }
        else
            printf("File opened for writing\n");

        //pt=0x10000;
        pt=0;
        search=0;

        fseek(fp1,pt,0);
        fread(&sk,1,1,fp1);
        while (search!=1)
        {
            while (sk!='r')
            {
                pt++;
                fseek(fp1,pt,0);
                fread(&sk,1,1,fp1);
            }
            //printf("r found %X\n",pt);

            fseek(fp1,(pt+1),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='e');
            fseek(fp1,(pt+2),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='c');
            fseek(fp1,(pt+3),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='o');
            fseek(fp1,(pt+4),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='v');
            fseek(fp1,(pt+5),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='e');
            fseek(fp1,(pt+6),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='r');
            fseek(fp1,(pt+7),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='y');
            fseek(fp1,(pt+8),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='.');
            fseek(fp1,(pt+9),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='f');
            fseek(fp1,(pt+10),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='e');
            fseek(fp1,(pt+11),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='x')-10;
            if (search!=1)
            {
                search=0;
                pt=pt+1;
                fseek(fp1,pt,0);
                fread(&sk,1,1,fp1);
            }

        }
        //printf("Recovery.img found %X\n",pt); //printf("STR found %X\n",pt);
        //getch();

        lth=0;
        fseek(fp1,(pt+0x100),0);
        fread(&sk,1,1,fp1);
        lth=lth+sk;
        fread(&sk,1,1,fp1);
        lth=lth+sk*0x100;
        fread(&sk,1,1,fp1);
        lth=lth+sk*0x10000;
        fread(&sk,1,1,fp1);
        lth=lth+sk*0x1000000;
        if (sk==0) lth=lth%0x1000000;
        //printf("lth found %X\n",lth);
        //getch();

        strt=0;
        fseek(fp1,(pt+0x100+0x10),0);
        fread(&sk,1,1,fp1);
        strt=strt+sk;
        fread(&sk,1,1,fp1);
        strt=strt+sk*0x100;
        fread(&sk,1,1,fp1);
        strt=strt+sk*0x10000;
        fread(&sk,1,1,fp1);
        strt=strt+sk*0x1000000;
        if (sk==0) strt=strt%0x1000000;
        //printf("Strt found %X\n",strt);
        //getch();


        fseek(fp1,strt,0);
        while (lth>1000000)
        {
            fread(&fram,1000000,1,fp1);
            fwrite(&fram,1000000,1,fp2);
            lth=lth-1000000;
        }
        fread(&fram,lth,1,fp1);
        fwrite(&fram,lth,1,fp2);
        fclose(fp2);

        printf("Recovery.img is ready!\n");




        if((fp2=fopen("system.img","wb"))==NULL)
        {
            printf("File cannot be opened\n");
            exit(0);
        }
        else
            printf("File opened for writing\n");

        //pt=0x10000;
        pt=0;
        search=0;

        fseek(fp1,pt,0);
        fread(&sk,1,1,fp1);
        while (search!=1)
        {
            while (sk!='s')
            {
                pt++;
                fseek(fp1,pt,0);
                fread(&sk,1,1,fp1);
            }
            //printf("r found %X\n",pt);

            fseek(fp1,(pt+1),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='y');
            fseek(fp1,(pt+2),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='s');
            fseek(fp1,(pt+3),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='t');
            fseek(fp1,(pt+4),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='e');
            fseek(fp1,(pt+5),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='m');
            fseek(fp1,(pt+6),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='.');
            fseek(fp1,(pt+7),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='f');
            fseek(fp1,(pt+8),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='e');
            fseek(fp1,(pt+9),0);
            fread(&sk,1,1,fp1);
            search=search+(sk=='x')-8;
            if (search!=1)
            {
                search=0;
                pt=pt+1;
                fseek(fp1,pt,0);
                fread(&sk,1,1,fp1);
            }

        }
        //printf("System.img found %X\n",pt); //printf("STR found %X\n",pt);
        //getch();

        lth=0;
        fseek(fp1,(pt+0x100),0);
        fread(&sk,1,1,fp1);
        lth=lth+sk;
        fread(&sk,1,1,fp1);
        lth=lth+sk*0x100;
        fread(&sk,1,1,fp1);
        lth=lth+sk*0x10000;
        fread(&sk,1,1,fp1);
        lth=lth+sk*0x1000000;
        if (sk==0) lth=lth%0x1000000;
        //printf("lth found %X\n",lth);
        //getch();

        strt=0;
        fseek(fp1,(pt+0x100+0x10),0);
        fread(&sk,1,1,fp1);
        strt=strt+sk;
        fread(&sk,1,1,fp1);
        strt=strt+sk*0x100;
        fread(&sk,1,1,fp1);
        strt=strt+sk*0x10000;
        fread(&sk,1,1,fp1);
        strt=strt+sk*0x1000000;
        if (sk==0) strt=strt%0x1000000;
        //printf("Strt found %X\n",strt);
        //getch();


        fseek(fp1,strt,0);
        while (lth>1000000)
        {
            fread(&fram,1000000,1,fp1);
            fwrite(&fram,1000000,1,fp2);
            lth=lth-1000000;
        }
        fread(&fram,lth,1,fp1);
        fwrite(&fram,lth,1,fp2);
        fclose(fp2);

        printf("System.img is ready!\n");





        fclose(fp1);
    }



    //fix system
    //system
    {
        if((inFile=fopen("system.img","rb+"))==NULL)
        {
            printf("File system.img cannot be opened\n");
            exit(0);
        }
        else
            printf("File opened for reading\n");

        if((outFile=fopen("systemrc.img","wb"))==NULL)
        {
            printf("File cannot be opened\n");
            exit(0);
        }
        else
            printf("File opened for writing\n");

        fseek(inFile, 0, SEEK_END);
        filesize = ftell(inFile);

        fseek(inFile, 0x1C, SEEK_SET);
        do {
            fread(&info, 1, sizeof(info), inFile);
            if (info.magic == 0x0000CAC1)
            {
                count = info.size - sizeof(info);
                pmem = malloc(count);
                fread(pmem, 1, count, inFile);
                fwrite(pmem, 1, count, outFile);
                free(pmem);
            }
            else if (info.magic == 0x0000CAC3)
            {
                fseek(outFile, info.count * 0x1000-1, SEEK_CUR);
                count = 0;
                fwrite(&count, 1, 1, outFile);
            }
            else
                break;


        }
        while (ftell(inFile)<filesize);

        fclose(inFile);
        fclose(outFile);
        //system

        //getchar();
    }



    if ((argc!=1) && (argv[1]=="wozhidaolimianmeiyoucaidanshibukenengdeba"))
    {
        printf("通过加参数以后看到这句话的人，想必不多吧，呵呵。既然看到了，那就不是外人了，无妨。\n从580r v1开始，国产mp4大量进入android时代，我是11年元旦买的580r。580r支持otg，可以插鼠标u盘什么的，但rk2816阉割了网络功能，很多人表示这个不能忍。买的时候我就在想，既然支持otg，为什么不可以插无线网看？大不了自己加个驱动就是了啊。买来之后就是……期末考试……（众人：倒……）稍微闲置了一段时间以后，我的第一个android设备就这样开始折腾了。\n玩家固件小组也是在这个时候诞生，当时应该叫rk28玩家固件小组吧。小组建立的时候目的大概就是为了搞定rk2816的网络问题，呵呵，但事实证明，这群孩子真的是很傻很天真，这件事待会儿再说。小组成立一段时间以后，我也加到小组群中，也无非讨论网络部分加进去怎么地，又有什么问题。后来从牛肉松那里学到了1.5固件和2.1固件的解包打包方法什么的。1.5固件好说，论坛里有人发了工具。2.1固件rk做了限制，需要手动修改打包后文件的6个字节（当时是14个吧）来“迷惑”刷机工具。这种纯手动的工作，不仅费时费力，而且容易出错。580r没有menu键，刷挂了就得拆机短接flash芯片，所以很是郁闷。在群里吼了一嗓子谁做个工具，没人搭理，那就自己做得了，反正c pascal delphi python神马的都看过一点点，谷歌一下写个这种程序也花不了多长时间。于是我做的rk28 android 2.1的diy工具也就诞生。期间，还做了刷机教程以及diy教程。debian也是那段时间尝试运行在了rk28上，虽然不是原生运行。众所周知，rk28的后一代就是rk29咯，昂达第一个rk2918产品便是vi30（旗舰），8寸显示屏也是昂达出的第一个真正意义上的平板吧，宣传做的挺足的，不过这机器在人们印象里就是个废，昂达从说了要发布开始，拖了都快半年吧，原因当然要归功于瑞芯微rk了，想必这个被坑掉的旗舰也是昂达和瑞芯微决裂的主要原因吧，目测此后rk出的rk30 rk31昂达也是没有用的欲望了。芯片的更替，小组名字也从rk28玩家固件小组变成rk玩家固件小组，由于组中有多名非昂达产品玩家，（比如好基友dehepo）所以小组一直没有以昂达来命名。那么说到这里，继续说rk2816网络的故事，随着研究的不断深入，我们发现rk做的真够绝的，添加完驱动以后仍然不能使用，甚至只添加tun.ko模块都不行。同样是rk28，rk2818的580w添加tun.ko用debian加载openvpn可以正常的做服务端和客户端，而rk2816只要开启openvpn服务系统就直接重启，很明显rk在网络部分做手脚了。之后的研究就集中于内核的hack，由于580w屏幕摔坏，580r屏幕拆到580w上用着，所以我也就再没关心过rk2816的进展，毕竟这块号称过1G主频的破u，是armv5的600mhz，烂到无以复加。而rk29和rk28相似的是，rk29一直限制system格式，导致system不可写，这在android届也算是一朵奇葩了。所谓的sdk2.0完全就是一个噱头，是把部分限制打开，然后加了个android4桌面的彻头彻尾的噱头！是把自己吃过屎的嘴轻轻抹去翔迹以后还向人炫耀饱嗝的奇葩之作！我佩服你。号称1.2G的主频其实只有1G，我非常佩服你。rk28、rk29的内核源码都是在国外找到的，至于android层源码，当我没说。这种国产厂商的诚意我也就不多说什么了。为迎接vi30的发布，小组早就下好固件开始研究，也第一时间做出了rk29解包打包工具，核心文件依旧是我写的，snakegtx901给出的方案。这个工具是我写出来非常满意的工具，从sdk1到sdk2，从boot到recovery，都支持，也算是我为rk机型做的最后一点东西了吧。\nrk吐槽完，该吐槽全志了。单核时代，昂达先发布rk29机型，再换个姿势发布A10的同样机型，昂达在想什么……A10虽然比rk29晚，但是由于集成度高，成本低，一下扫清了rk28残余势力，对rk29冲击也是显而易见。而对我们固件小组成员来说，最感觉欣慰的是，A10源代码后来开放出来了，这也使得国内国外diy固件层出不穷。加上A10的电视端，也就是m802的出现，使得A10的原生linux开发进展迅速，diy一片兴兴向荣的样子。不过A10并不完美，号称1.5G主频也只有1G，可以cpuset到1.2G，再高就死机。A10末期又推出A13，还有国内很少人知道的A10s，整个就是在炒冷饭，512m的内存限制也是让人非常头疼。好在A10机型大都是1G内存，我手里也没有A13机器，也就忍了。说完机型继续说自己。我拿到vi30的时候，已经开始大四的学业了，A10更实在大四中，毕业设计每天都得做，所以实在是没有什么经历分配到A10上。所以从我这里，A10机型基本没有什么贡献，在这里也是对昂达的机友说一声对不住了。直到前不久才发了原生linux+android双系统固件，算是补偿吧。\n说道这里，说说我自己了。从时间轴来看，拿到rk29的时候我大四，拿到A10的vi10精英版也是在大四，这时候基本处于酱油状态吧，也就是参与小组的讨论，但很难分配时间来diy固件什么的。固件小组依然有很多人在默默的做固件，我也希望有更多的人能够加入到diy的行列中，毕竟做固件不容易，不是谁想学就能学会，国内小白太多，哎……以后我仍在在固件小组当中，目前在读研究生，毕业以后也就得参加工作了，以后是酱油还是什么，再说吧。昂达双核机型我就没有碰过，如今A31都发布了，感慨万千。A31diy工具依然是snakegtx901提供的解包方案，我来编程实现，后续的整套工具的实现由snakegtx901完成。为A31，我大概只能做到这么多了吧。。\n最后吐槽一下国内的玩机环境。玩家固件小组顾名思义就是玩家咯，把有能力有兴趣做固件的人集中起来办事，也就是这样的思想吧。必要的时候，昂达会有机器提供测试什么的，也算不错了。ondabbs.cn也大概是半官方版民间的论坛，不过确实是国内最大的昂达平板交流论坛，我的这些故事都发生在这里。至于槽点呢~呵呵。中国人多，网络这个地方说话又不需要负责人，所以造就了一代又一代的网络暴民。当然，水军更是缺少不了的。各种诬陷，各种所谓的爆料，各种网络攻击ddos什么的，见怪不怪了。ondabbs的环境还需要大家维护，diy圈子的环境也需要大家来维护。给平板做固件的人本来就不多，厂商的支持也有限，我们也没有从中获得收入。既然我已经决定打酱油，那么这些磨嘴皮子的事情也就没我的事了，倒是想对某些厂商的水军说一句话，你有那时间往别人脸上扣屎盆子，还不如把自己的屁股擦干净了。至于那些小白，就这样了吧，我不是来拯救小白的，没人可以。连刷机都不会，买来不顺眼就开喷，想要苹果的体验，请卖掉你手中的国产板子，花钱买个白色的apple。在论坛里你就是喊破喉咙都没人搭理你，还不如自己找找你自己的问题，很多问题都是小白没用过android什么的……\n最后，希望所有国产的厂家，能够做好自己的产品，做出真正拿得出手的东西来给世界看看。什么？我说的不是价格，是产品啊！是硬件和软件的良好结合啊混蛋！\n感谢面包、字符、浪味仙、兽兽、探花、薯条等等在小组里陪伴了这么久的人，我还会在小组里和你们扯蛋的。\n2013 02 15\nbearqq\nbbbbqqq@gmail.com\n");
        printf("\nPress ant key to exit.\n");
        getchar();
    }
}
