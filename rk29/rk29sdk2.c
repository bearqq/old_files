#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

//void sdk2()
//{
//    ShellExecute(NULL, NULL, "sdk2.bat", NULL, NULL,SW_SHOW);
//}
//
//void sdk1()
//{
//    ShellExecute(NULL, NULL, "sdk1.bat", NULL, NULL,SW_SHOW);
//}

void Seprecov()
{
    FILE *fp1,*fp2,*fp3;
    char fram[1000001];
    unsigned long pt,lth;
    short search;


    if((fp1=fopen("temp\\Image\\recovery.img","rb+"))==NULL)
    {
        printf("recovery.img cannot beopened\n");
        exit(0);
    }
    else
        printf("recovery.img opened for writing\n");

    search=0;
    pt=0x0;
    fseek(fp1,pt,0);
    fread(&fram,1,1,fp1);
    search=search+((fram[0]==0x41)||(fram[0]==(-256+0x41)));
    fread(&fram,1,1,fp1);
    search=search+((fram[0]==0x4e)||(fram[0]==(-256+0x4e)));
    fread(&fram,1,1,fp1);
    search=search+((fram[0]==0x44)||(fram[0]==(-256+0x44)));
    fread(&fram,1,1,fp1);
    search=search+((fram[0]==0x52)||(fram[0]==(-256+0x52)))-3;

    if (search==1)
    {


        if((fp2=fopen("temp\\Image\\recovery_out.img","wb"))==NULL)
        {
            printf("boot_out.img cannot beopened\n");
            exit(0);
        }
        else
            printf("boot_out.img opened for writing\n");


        if((fp3=fopen("temp\\Image\\kernel_out.img","wb"))==NULL)
        {
            printf("kernel_out.img cannot beopened\n");
            exit(0);
        }
        else
            printf("kernel_out.img opened for writing\n");


        printf("\nNow Seperate Recovery from Recovery.\n");

        pt=0;
        lth=0;
        fseek(fp1,0x10,0);
        fread(&lth,1,1,fp1);
        fread(&pt,1,1,fp1);
        lth=lth+pt*0x100;
        fread(&pt,1,1,fp1);
        lth=lth+pt*0x10000;
        fread(&pt,1,1,fp1);
        lth=lth+pt*0x1000000;
        //printf("%X\n",lth);

        pt=0;
        search=0;
        fseek(fp1,pt,0);
        fread(&fram,1,1,fp1);
        while (search!=1)
        {

            while ((fram[0]!=0x1f)&&(fram[0]!=(-256+0x1f)))
            {
                pt++;
                fread(&fram,1,1,fp1);
            }

            fread(&fram,1,1,fp1);
            search=search+((fram[0]==0x8b)||(fram[0]==(-256+0x8b)));
            //printf("debug info:pt=%X  fram=%X   %d\n",pt+1,fram[0],((fram[0]==0x8b)||(fram[0]==(-256+0x8b))));
            fread(&fram,1,1,fp1);
            search=search+((fram[0]==0x08)||(fram[0]==(-256+0x08)));
            fread(&fram,1,1,fp1);
            search=search+((fram[0]==0x00)||(fram[0]==(-256+0x00)));
            fread(&fram,1,1,fp1);
            search=search+((fram[0]==0x00)||(fram[0]==(-256+0x00)))-3;
            //printf("debug info:pt=%X  fram=%X   %d\n",pt+2,fram[0],((fram[0]==0x08)||(fram[0]==(-256+0x08))));
            //fread(&fram,1,1,fp1);
            //search=search+((fram[0]!=0xe3)||(fram[0]!=(-256+0xe3)))-2;

            if (search!=1)
            {
                search=0;
                pt=pt+1;
                fseek(fp1,pt,0);
                fread(&fram,1,1,fp1);
            }

        }
        printf("debug info:Recovery found\n");

        //printf("debug info:pt=%X\n",pt);//for debug


        fseek(fp1,pt,0);
        while (lth>1000000)
        {
            fread(&fram,1000000,1,fp1);
            fwrite(&fram,1000000,1,fp2);
            lth=lth-1000000;
        }
        fread(&fram,lth,1,fp1);
        fwrite(&fram,lth,1,fp2);


        pt=0;
        lth=0;
        fseek(fp1,8,0);
        fread(&lth,1,1,fp1);
        fread(&pt,1,1,fp1);
        lth=lth+pt*0x100;
        fread(&pt,1,1,fp1);
        lth=lth+pt*0x10000;
        fread(&pt,1,1,fp1);
        lth=lth+pt*0x1000000;
        //printf("%X\n",lth);

        pt=0;
        search=0;
        fseek(fp1,pt,0);
        fread(&fram,1,1,fp1);
        while (search!=1)
        {
            while ((fram[0]!=0xd3)&&(fram[0]!=(-256+0xd3)))
            {
                pt++;
                fread(&fram,1,1,fp1);
            }
            fread(&fram,1,1,fp1);
            search=search+((fram[0]==0xf0)||(fram[0]==(-256+0xf0)));
            fread(&fram,1,1,fp1);
            search=search+((fram[0]==0x21)||(fram[0]==(-256+0x21)));
            fread(&fram,1,1,fp1);
            search=search+((fram[0]==0xe3)||(fram[0]==(-256+0xe3)))-2;

            if (search!=1)
            {
                search=0;
                pt=pt+1;
                fseek(fp1,pt,0);
                fread(&fram,1,1,fp1);
            }

        }
        printf("debug info:Kernel found\n");

        //printf("debug info:pt=%X\n",pt);//for debug


        fseek(fp1,pt,0);
        while (lth>1000000)
        {
            fread(&fram,1000000,1,fp1);
            fwrite(&fram,1000000,1,fp3);
            lth=lth-1000000;
        }
        fread(&fram,lth,1,fp1);
        fwrite(&fram,lth,1,fp3);

        //fseek(fp2,0,2);
        //printf("%X\n",tell(fp2));
        printf("Success\n");

        ShellExecute(NULL, NULL, "LRECOVERY.bat", NULL, NULL,SW_SHOW);

        fclose(fp2);
        //fclose(fp3);
    }
    fclose(fp1);
}

int main()
{
    FILE *fp1,*fp2,*fp3;
    char fram[1000001];
    unsigned long pt,lth;
    short search;

    Seprecov();

    if((fp1=fopen("temp\\Image\\boot.img","rb+"))==NULL)
    {
        printf("boot.img cannot beopened\n");
        exit(0);
    }
    else
        printf("boot.img opened for writing\n");


    search=0;
    pt=0x0;
    fseek(fp1,pt,0);
    fread(&fram,1,1,fp1);
    search=search+((fram[0]==0x41)||(fram[0]==(-256+0x41)));
    fread(&fram,1,1,fp1);
    search=search+((fram[0]==0x4e)||(fram[0]==(-256+0x4e)));
    fread(&fram,1,1,fp1);
    search=search+((fram[0]==0x44)||(fram[0]==(-256+0x44)));
    fread(&fram,1,1,fp1);
    search=search+((fram[0]==0x52)||(fram[0]==(-256+0x52)))-3;

    if (search==1)
    {



        if((fp2=fopen("temp\\Image\\boot_out.img","wb"))==NULL)
        {
            printf("boot_out.img cannot beopened\n");
            exit(0);
        }
        else
            printf("boot_out.img opened for writing\n");


        if((fp3=fopen("temp\\Image\\kernel_out.img","wb"))==NULL)
        {
            printf("kernel_out.img cannot beopened\n");
            exit(0);
        }
        else
            printf("kernel_out.img opened for writing\n");

        printf("\nNow Seperate Kernel from Boot.\n");

        pt=0;
        lth=0;
        fseek(fp1,0x10,0);
        fread(&lth,1,1,fp1);
        fread(&pt,1,1,fp1);
        lth=lth+pt*0x100;
        fread(&pt,1,1,fp1);
        lth=lth+pt*0x10000;
        fread(&pt,1,1,fp1);
        lth=lth+pt*0x1000000;
        //printf("%X\n",lth);

        pt=0;
        search=0;
        fseek(fp1,pt,0);
        fread(&fram,1,1,fp1);
        while (search!=1)
        {

            while ((fram[0]!=0x1f)&&(fram[0]!=(-256+0x1f)))
            {
                pt++;
                fread(&fram,1,1,fp1);
            }

            fread(&fram,1,1,fp1);
            search=search+((fram[0]==0x8b)||(fram[0]==(-256+0x8b)));
            //printf("debug info:pt=%X  fram=%X   %d\n",pt+1,fram[0],((fram[0]==0x8b)||(fram[0]==(-256+0x8b))));
            fread(&fram,1,1,fp1);
            search=search+((fram[0]==0x08)||(fram[0]==(-256+0x08)));
            fread(&fram,1,1,fp1);
            search=search+((fram[0]==0x00)||(fram[0]==(-256+0x00)));
            fread(&fram,1,1,fp1);
            search=search+((fram[0]==0x00)||(fram[0]==(-256+0x00)))-3;
            //printf("debug info:pt=%X  fram=%X   %d\n",pt+2,fram[0],((fram[0]==0x08)||(fram[0]==(-256+0x08))));
            //fread(&fram,1,1,fp1);
            //search=search+((fram[0]!=0xe3)||(fram[0]!=(-256+0xe3)))-2;

            if (search!=1)
            {
                search=0;
                pt=pt+1;
                fseek(fp1,pt,0);
                fread(&fram,1,1,fp1);
            }

        }
        printf("debug info:Boot found\n");


        fseek(fp1,pt,0);
        while (lth>1000000)
        {
            fread(&fram,1000000,1,fp1);
            fwrite(&fram,1000000,1,fp2);
            lth=lth-1000000;
        }
        fread(&fram,lth,1,fp1);
        fwrite(&fram,lth,1,fp2);


        pt=0;
        lth=0;
        fseek(fp1,8,0);
        fread(&lth,1,1,fp1);
        fread(&pt,1,1,fp1);
        lth=lth+pt*0x100;
        fread(&pt,1,1,fp1);
        lth=lth+pt*0x10000;
        fread(&pt,1,1,fp1);
        lth=lth+pt*0x1000000;
        //printf("%X\n",lth);

        pt=0;
        search=0;
        fseek(fp1,pt,0);
        fread(&fram,1,1,fp1);
        while (search!=1)
        {
            while ((fram[0]!=0xd3)&&(fram[0]!=(-256+0xd3)))
            {
                pt++;
                fread(&fram,1,1,fp1);
            }
            fread(&fram,1,1,fp1);
            search=search+((fram[0]==0xf0)||(fram[0]==(-256+0xf0)));
            fread(&fram,1,1,fp1);
            search=search+((fram[0]==0x21)||(fram[0]==(-256+0x21)));
            fread(&fram,1,1,fp1);
            search=search+((fram[0]==0xe3)||(fram[0]==(-256+0xe3)))-2;

            if (search!=1)
            {
                search=0;
                pt=pt+1;
                fseek(fp1,pt,0);
                fread(&fram,1,1,fp1);
            }

        }
        printf("debug info:Kernel found\n");


        fseek(fp1,pt,0);
        while (lth>1000000)
        {
            fread(&fram,1000000,1,fp1);
            fwrite(&fram,1000000,1,fp3);
            lth=lth-1000000;
        }
        fread(&fram,lth,1,fp1);
        fwrite(&fram,lth,1,fp3);

        //fseek(fp2,0,2);
        //printf("%X\n",tell(fp2));
        printf("Success\n");


        ShellExecute(NULL, NULL, "LBOOT.bat", NULL, NULL,SW_SHOW); //sdk2 seperate boot

        fclose(fp2);
        fclose(fp3);
    }
    else
    {
        FILE *fp4;

        if((fp4=fopen("temp\\Image\\system.img","rb+"))==NULL)
            {
                printf("system.img cannot beopened\n");
                exit(0);
            }
        else
            printf("system.img opened for writing\n");

        printf("\nSeperated\n"); //boot correct

        search=0;
        pt=0x10;
        fseek(fp4,pt,0);
        fread(&fram,1,1,fp4);
        search=search+((fram[0]==0x43)||(fram[0]==(-256+0x43)));
        fread(&fram,1,1,fp4);
        search=search+((fram[0]==0x6f)||(fram[0]==(-256+0x6f)));
        fread(&fram,1,1,fp4);
        search=search+((fram[0]==0x6d)||(fram[0]==(-256+0x6d)));
        fread(&fram,1,1,fp4);
        search=search+((fram[0]==0x70)||(fram[0]==(-256+0x70)))-3;

        if (search!=1)
        {
            ShellExecute(NULL, NULL, "NBOOT_ext3.bat", NULL, NULL,SW_SHOW); //sdk1 or sdk2 ext3 system
        }
        else
        {
            ShellExecute(NULL, NULL, "NBOOT_cramfs.bat", NULL, NULL,SW_SHOW); //sdk1 cramfs system
        }

        fclose(fp4);

    }

    fclose(fp1);

    //getchar(); //for debug
}
