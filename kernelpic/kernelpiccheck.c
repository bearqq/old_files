#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE *fp1,*fp2,*bmp;
    char fram[1000001];
    unsigned long pt,lth,remem,seshu,tezheng,bmppt;
    short search;

    if((fp1=fopen("kernel.img","rb+"))==NULL)
    {
        printf("File cannot beopened\n");
        exit(0);
    }
    else
        printf("File opened for writing\n");

    fp2=fopen("kernel2.img","wb+");
    fclose(fp2);

    if((fp2=fopen("kernel2.img","rb+"))==NULL)
    {
        printf("File cannot beopened\n");
        exit(0);
    }
    else
        printf("File opened for writing\n");

    //--------------------step1-----------------------------
    //读取04-07，记录到remem
    fseek(fp1,4,0);
    fread(&remem,1,1,fp1);
    //printf("debug info:remem:%X\n",remem);
    fread(&pt,1,1,fp1);
    remem=remem+pt*0x100;
    //printf("debug info:remem:%X,%X\n",pt,remem);
    fread(&pt,1,1,fp1);
    remem=remem+pt*0x10000;
    //printf("debug info:remem:%X,%X\n",pt,remem);
    fread(&pt,1,1,fp1);
    remem=remem+pt*0x1000000;
    printf("debug info:remem:%X\n",remem);

    //--------------------step2~6-----------------------------
    //删除前8，删去remem以后
    //fseek(fp1,0,2);   /*将文件指针从文件头移动文件尾*/
    //lth=ftell(fp1);   /*检测文件当前指针位置，求得文件长度*/
    lth=remem;

    fseek(fp1,8,0);
    while (lth>1000000)
    {
        //seek(fp1,pt,0);
        fread(&fram,1000000,1,fp1);
        fwrite(&fram,1000000,1,fp2);
        lth=lth-1000000;
        //pt=pt+1000000;
        //printf("did!\n");
    }
    fread(&fram,lth,1,fp1);
    fwrite(&fram,lth,1,fp2);
//
//    //--------------------step7-----------------------------
//    //查找16进制串0300000020030000E0010000，这里姑且叫它特征串,地址tezheng
//    pt=0x18000;
//    search=0;
//    fseek(fp2,pt,0);
//    fread(&fram,1,1,fp2);
//    while (search!=1)
//    {
//        while (fram[0]!=0x03)
//        {
//            pt++;
//            fseek(fp2,pt,0);
//            fread(&fram,1,1,fp2);
//        }
//        //printf("debug info:first chapter found %X\n",pt);
//        //fseek(fp1,(pt+1),0);
//        fread(&fram,3,1,fp2);
//        search=search+(0x00==(fram[0]+fram[1]+fram[2]));
//        //fseek(fp1,(pt+2),0);
//        fread(&fram,1,1,fp2);
//        search=search+(fram[0]==0x20);
//        //fseek(fp1,(pt+3),0);
//        fread(&fram,1,1,fp2);
//        search=search+(fram[0]==3)-2;
//        if (search!=1)
//        {
//            search=0;
//            pt=pt+1;
//            fseek(fp2,pt,0);
//            fread(&fram,1,1,fp2);
//        }
//
//    }
//    printf("debug info:Chapter found %X\n",pt);
//    tezheng=pt;
//
//    //--------------------step8-----------------------------
//    //查找色板颜色数seshu
//    fseek(fp2,tezheng+0xc,0);
//    fread(&seshu,1,1,fp2);
//    printf("Importance!:Seshu 16=%x 10=%d\n",seshu,seshu);
//    printf("Are you ready? You need a kernel.bmp!\n");
//    getchar();
//    printf("Last chance, are you ready?\n");
//    getchar();
//
//    //--------------------step9-----------------------------
//    //tezheng，seshu。tezheng+0x18=调色板起始。调色板数据=seshu*3。调色板起始+4+调色板数据数据（-2） 终止-1 为像素数据起始，大小384000
//    if((bmp=fopen("kernel.bmp","rb+"))==NULL)
//    {
//        printf("File cannot beopened\n");
//        exit(0);
//    }
//    else
//        printf("File opened for writing\n");
//
//    pt=0;
//    fseek(bmp,0xa,0);
//    fread(&bmppt,1,1,bmp);
//    //printf("debug info:bmppt:%X\n",bmppt);
//    fread(&pt,1,1,bmp);
//    bmppt=bmppt+pt*0x100;
//    //printf("debug info:bmppt:%X,%X\n",pt,bmppt);
//    printf("debug info:bmppt:%X\n",bmppt);
//    if ((bmppt-0x36)!=(seshu*4))
//    {
//        printf("ERROR! Continue? Not recommended!\nCheck BMP!\n"); //核对数据！
//        getchar();
//    }
//
//    //--------------------step10-----------------------------
//    //写入像素信息
//    lth=800*480;
//    //printf("debug info:length %d\n",lth);
//    printf("debug info:start %X\n",tezheng+0x18+seshu*3+2);
//    fseek(bmp,bmppt,0);
//    fseek(fp2,(tezheng+0x18+seshu*3+2),0);
//    while (lth>1000000)
//    {
//        //seek(fp1,pt,0);
//        fread(&fram,1000000,1,bmp);
//        for (pt=0;pt<1000000;pt++)
//            fram[pt]=fram[pt]+0x20;
//        fwrite(&fram,1000000,1,fp2);
//        lth=lth-1000000;
//        //pt=pt+1000000;
//    }
//    fread(&fram,lth,1,bmp);
//    for (pt=0;pt<lth;pt++)
//        fram[pt]=fram[pt]+0x20;
//    fwrite(&fram,lth,1,fp2);
//
//    //--------------------step10-----------------------------
//    //写入色板信息
//    fseek(bmp,0x36,0);
//    fseek(fp2,(tezheng+0x18),0);
//    for (pt=1;pt<=seshu;pt++)
//    {
//        fread(&fram,4,1,bmp);
//        {
//            fram[6]=fram[0];
//            fram[0]=fram[2];
//            fram[2]=fram[6];
//        }
//        fwrite(&fram,3,1,fp2);
//        //printf("%d\n",pt);
//    }







//finish
    fclose(fp1);
    fclose(fp2);
//    fclose(bmp);

//debug
    printf("All Done! Press any key to quit.\n");
    getchar();
}
