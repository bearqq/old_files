#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE *fp1,*fp2;
    char fram[1000001];
    unsigned long pt,lth,remem;

    if((fp1=fopen("kernel.img","rb+"))==NULL)
    {
        printf("File cannot beopened\n");
        exit(0);
    }
    else
        printf("File opened for writing\n");

    //fp2=fopen("kernel2.img","wb+");
    //fclose(fp2);

    if((fp2=fopen("kernel_new.img","rb+"))==NULL)
    {
        printf("File cannot beopened\n");
        exit(0);
    }
    else
        printf("File opened for writing\n");

    //--------------------step1-----------------------------
    //读取04-07，记录到remem
    fseek(fp1,4,0);
    fread(&remem,1,1,fp1);
    //printf("debug info:remem:%X\n",remem);
    fread(&pt,1,1,fp1);
    remem=remem+pt*0x100;
    //printf("debug info:remem:%X,%X\n",pt,remem);
    fread(&pt,1,1,fp1);
    remem=remem+pt*0x10000;
    //printf("debug info:remem:%X,%X\n",pt,remem);
    fread(&pt,1,1,fp1);
    remem=remem+pt*0x1000000;
    printf("debug info:remem:%X\n",remem);

    //--------------------step2~6-----------------------------
    //删除前8，删去remem以后
    //fseek(fp1,0,2);   /*将文件指针从文件头移动文件尾*/
    //lth=ftell(fp1);   /*检测文件当前指针位置，求得文件长度*/

    fseek(fp2,0,2);   /*将文件指针从文件头移动文件尾*/
    lth=ftell(fp2);   /*检测文件当前指针位置，求得文件长度*/
    fseek(fp2,lth-4,0);

    fseek(fp1,0,2);   /*将文件指针从文件头移动文件尾*/
    lth=ftell(fp1);   /*检测文件当前指针位置，求得文件长度*/
    lth=lth-remem;
    fseek(fp1,remem,0);

    while (lth>1000000)
    {
        //seek(fp1,pt,0);
        fread(&fram,1000000,1,fp1);
        fwrite(&fram,1000000,1,fp2);
        lth=lth-1000000;
        //pt=pt+1000000;
        //printf("did!\n");
    }
    fread(&fram,lth,1,fp1);
    fwrite(&fram,lth,1,fp2);

//finish
    fclose(fp1);
    fclose(fp2);
    //fclose(bmp);

//debug
    printf("All Done! Press any key to quit.\n");
    getchar();
}
